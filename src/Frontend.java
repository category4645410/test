public interface Frontend {
    String CategoryMenu =
            "1. Create Category \t" +
            "2. Delete Category \t" +
            "3. Set Category to another \t" +
            "4. Menu \t" +
            "5. Show all Categories \t" +
            "6. Product Details \t" +
            "0. Exit \n" +
            "Choose option: ";
}
