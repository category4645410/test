package model;

public class Product extends Base {
    private double price;

    public Product(String name, double price) {
        super.name = name;
        this.price = price;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }
}
