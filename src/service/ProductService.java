package service;

import model.Product;

public class ProductService extends BaseService {
    private Product[] products = new Product[1000];
    private int indexProducts;

    @Override
    public boolean add(Object object) {
        Product product = (Product) object;

        if (!validate(product)) {
            return false;
        }

        products[indexProducts++] = product;

        return true;
    }

    private boolean validate(Object object) {
        Product product = (Product) object;

        return (product != null && !isExist(product.getName()));
    }

    private boolean isExist(String name) {
        if (name.isEmpty()) {
            return false;
        }

        for (Product product: products) {
            if (product != null && product.getName().equals(name)) {
                return true;
            }
        }

        return false;
    }


}
